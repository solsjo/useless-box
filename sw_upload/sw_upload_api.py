"""SW upload API."""

import pynqc
import pynqc.raw_functions as raw_func


def monitor_state(rf, states, callback):
    """Read out the states specified in the argument."""
    cont = True
    for state in states:
        source = int(state["key"])
        index = int(state["value"])
        cont = callback(source, index, rf.get_value(source, index))
        if not cont:
            break
    return cont


def _validate_interface(nqc, interface):
    """Validate values in the program."""
    rf = nqc.get_raw_functions()
    for key, value in interface.items():
        actual = rf.get_value(raw_func.VARIABLE, value["register"])
        if actual != value["value"]:
            return (False, key, actual, value["value"])
    return (True, None, None, None)


def _upload_firmware(nqc, firmware):
    """Download firmware if it does not already exist."""
    rf = nqc.get_raw_functions()
    version = rf.get_versions()["desc-ram"]
    if firmware["version"] != version:
        print(f"version: {version}, expected version:{firmware['version']}")
        print("Starting upload of firmware, this might take a while.")
        response = nqc.flash_firmware(firmware["path"])
        if response:
            print("Firmware upload complete")
            return 0
        else:
            print("Firmware upload failed")
            return 1
    print("Correct firmware version already installed")
    return 0


def _upload_program(nqc, program, flags):
    """Download program if it does not already exist."""
    print("uploading program")
    nqc.set_program_slot(program["slot"])
    rc = _validate_interface(nqc, program["interface"])
    if not rc[0]:
        print(f"{rc[1]}: {rc[2]} did not match {rc[3]}")
        response = nqc.flash_and_exec_application(program["path"], flags)
        if not response:
            print("Program upload failed")
            return 1

        # verify version again
        rc = _validate_interface(nqc, program["interface"])
        if not rc[0]:
            print(f"{rc[1]}: {rc[2]} did not match {rc[3]}")
            return 1
        else:
            print("Upload successful")
            return 0
    print("Program already installed")
    return 0


def _exec(nqc, program, callback):
    """Exec function for running program."""
    nqc.set_program_slot(program["slot"])
    rc = _validate_interface(nqc, program["interface"])
    if not rc[0]:
        assert rc[0], f"{rc[1]}: {rc[2]} did not match {rc[3]}"
    print("Starting program.")
    cmds = [nqc._get_device(), "-run"]
    data = pynqc.nqc._exec_nqc(cmds)
    rc = True if data == b"" else False
    if not rc:
        assert rc, "starting program failed"
    else:
        states = program["monitor_state"]
        if states and callback:
            rf = nqc.get_raw_functions()
            cont = True
            while cont:
                cont = monitor_state(rf, states, callback)
