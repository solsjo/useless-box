"""This module uploads and runs the specified NQC program."""

import argparse
import time
import yaml
import json
from jsonschema import validate
import os
import pynqc
import pynqc.raw_functions as raw_func
import sw_upload.sw_upload_api as sw_upload_api


class ParseKwargs(argparse.Action):
    """Parse key, value pairs from argparse."""

    def __call__(self, parser, namespace, values, option_string=None):
        """Implement interface function  __call__."""
        setattr(namespace, self.dest, list())
        for value in values:
            key, val = value.split(":")
            getattr(namespace, self.dest).append(
                {"key": key, "value": int(val) if val != "" else None}
            )


class ParseNestedKwargs(argparse.Action):
    """Parse nested key, value pairs from argparse.

    Input on the form: <obj>:<key>:<value> becomes
    [{<obj>:{<key>:<value>}}].
    """

    def __call__(self, parser, namespace, values, option_string=None):
        """Implement interface function  __call__."""
        setattr(namespace, self.dest, dict())
        for value in values:
            obj, key, val = value.split(":")

            getattr(namespace, self.dest)[obj] = {
                "register": int(key),
                "value": int(val),
            }


def monitor_callback(source, index, value):
    """Interface function for callback."""
    time.sleep(1)
    print(f"state of register {source}:{index}: {value}")
    return True


def setup_parser():
    """Build a parser."""
    sources = raw_func.op_codes["get_value"]["arguments"]["source"]
    source_map = {
        "VARIABLE": 0,
        "TIMER": 1,
        "IMMEDIATE": 2,
        "MOTOR_STATE": 3,
        "RANDOM": 4,
        "CURRENT_PROG": 8,
        "SENSOR_VALUE": 9,
        "SENSOR_TYPE": 10,
        "SENSOR_MODE": 11,
        "RAW_SENSOR_VALUE": 12,
        "BOOL_SENSOR_VALUE": 13,
        "CLOCK": 14,
        "MESSAGE": 15,
    }

    valid_monitor_state_sources = [
        f"{value} = {key}" for key, value in source_map.items() if value in sources
    ]
    parser = argparse.ArgumentParser(
        description=f"Valid sources to monitor are: {valid_monitor_state_sources}"
    )
    parser.add_argument("--serial-type", help="type of serial interface")
    parser.add_argument(
        "--device-path", help="path to serial device",
    )
    parser.add_argument("--config", help="path to config")
    parser.add_argument("--firmware-path", help="path to firmware")
    parser.add_argument("--firmware-version", help="firmware version")
    parser.add_argument("--program-path", help="path to program")
    parser.add_argument("--program-slot", help="program slot to upload the program to")
    parser.add_argument(
        "--flags",
        help="key:value separated, "
        "where key is a flag "
        "and value is the "
        "value if any",
        nargs="*",
        action=ParseKwargs,
    )

    parser.add_argument(
        "--interface",
        help="name:key:value separated, "
        "where key is a register "
        "index and value is the "
        "expected value",
        nargs="*",
        action=ParseNestedKwargs,
    )
    parser.add_argument(
        "--monitor-state",
        help="source state to monitor," "source:index",
        nargs="*",
        action=ParseKwargs,
    )
    parser.add_argument(
        "--run", help="Also start the program", default=False, action="store_true"
    )
    parser.add_argument(
        "--dry-run", help="print all settings", default=False, action="store_true"
    )

    return parser


def build_flags(flags_map):
    """Build flags from dicts."""
    flags = list()
    for flag_map in flags_map:
        if flag_map["value"] is None:
            flags.append(f"{flag_map['key']}")
        else:
            flags.append(f"{flag_map['key']}={flag_map['value']}")

    return flags


def _update_program_settings(settings, args):
    """Update settings dict with program settings."""
    if args.program_path:
        settings["program"]["path"] = args.program_path
    if args.program_slot:
        settings["program"]["slot"] = args.program_slot
    if args.flags:
        settings["program"]["flags"].extend(args.flags)
    if args.interface:
        settings["program"]["interface"].update(args.interface)
    if args.monitor_state:
        settings["program"]["monitor_state"] = args.monitor_state
    else:
        settings["program"]["monitor_state"] = []


def validate_input(args):
    """Validate input arguments and build settings."""
    schema = None

    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, "json_input_schema.json")
    with open(filename) as schema_file:
        schema = schema_file.read()

    default_settings = """
serial_type: "usb"
device: "/dev/usb/legousbtower0"
firmware:
  path: null
  version: null

program:
  path: null
  slot: null
  flags: []
  interface: null
  monitor_state: []
    """
    settings = yaml.safe_load(default_settings)
    try:
        if args.config:
            with open(args.config) as yaml_cfg:
                cfg = yaml.load(yaml_cfg, Loader=yaml.FullLoader)
                settings.update(cfg)
                if cfg["program"]["flags"]:
                    flags = cfg["program"]["flags"]
                    settings["program"]["flags"] = flags

        if args.firmware_path and args.firmware_version:
            settings["firmware"]["path"] = args.firmware_path
            settings["firmware"]["version"] = args.firmware_version
        if args.program_path or settings["program"]:
            _update_program_settings(settings, args)
        if args.serial_type or args.device_path:
            settings["serial_type"] = args.serial_type
            settings["device"] = args.device_path

        validate(
            instance=settings, schema=json.loads(schema),
        )
    except Exception as excep:
        print(f"bad input: {settings}\n{excep}")

    return settings


def main():
    """Validate arguments and run."""
    rc = 0
    parser = setup_parser()
    args = parser.parse_args()
    settings = validate_input(args)
    nqc = pynqc.Nqc(serial_type=settings["serial_type"], device=settings["device"])
    if (settings["firmware"] or settings["program"]) and not args.dry_run:
        if settings["firmware"]:
            rc = sw_upload_api._upload_firmware(nqc, settings["firmware"])
        if settings["program"] and rc == 0:
            sw_upload_api._upload_program(
                nqc, settings["program"], build_flags(settings["program"]["flags"])
            )
        if args.run and settings["program"] and rc == 0:
            sw_upload_api._exec(nqc, settings["program"], monitor_callback)
    elif args.dry_run:
        import pprint

        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(settings)
    else:
        print("no command given")
    exit(rc)


if __name__ == "__main__":
    main()
