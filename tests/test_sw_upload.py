"""SW upload tests"""

from unittest import mock
from collections import namedtuple

# from test_raw_functions import check_hw_availability
import pynqc as _nqc
import pynqc.raw_functions as _rf
import sw_upload.sw_upload_api as sw_upload
import sw_upload.sw_upload_cli as sw_upload_cli

nqc = _nqc.Nqc(serial_type="usb", device="/dev/usb/legousbtower0")
rf = _rf.RawFunctions(nqc)


class TestSWUpload:
    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    @mock.patch(
        "pynqc.raw_functions.RawFunctions.get_value", mock.Mock(side_effect=[0, 2])
    )
    def test_monitor_interface(self):
        def callback(source, index, value):
            assert source == _rf.VARIABLE
            assert index in [1, 2]
            assert value in [0, 2]
            return True

        states = [{"key": _rf.VARIABLE, "value": 1}, {"key": _rf.VARIABLE, "value": 2}]
        sw_upload.monitor_state(rf, states, callback)

        source = _rf.VARIABLE
        expected = [mock.call(source, 1), mock.call(source, 2)]
        rf.get_value.assert_has_calls(expected)

    @mock.patch(
        "pynqc.raw_functions.RawFunctions.get_value", mock.Mock(side_effect=[0, 2])
    )
    def test__validate_interface(self):
        register_1 = 0
        register_2 = 1
        interface = {
            "id": {"register": register_1, "value": 0},
            "version": {"register": register_2, "value": 2},
        }
        response = sw_upload._validate_interface(nqc, interface)
        if not response == (True, None, None, None):
            assert False, f"{response}"

        source = _rf.VARIABLE
        expected = [mock.call(source, register_1), mock.call(source, register_2)]
        rf.get_value.assert_has_calls(expected)

    @mock.patch(
        "pynqc.raw_functions.RawFunctions.get_versions",
        mock.Mock(side_effect=[{"desc-ram": "3.8"}]),
    )
    @mock.patch("pynqc.Nqc.flash_firmware", mock.Mock(side_effect=[True]))
    def test__upload_firmware(self):
        firmware = {"version": "3.9", "path": "./firmware_path.lgo"}
        response = sw_upload._upload_firmware(nqc, firmware)
        if not response == 0:
            assert False, f"{response}"

        expected = [mock.call("./firmware_path.lgo")]
        nqc.flash_firmware.assert_has_calls(expected)

    @mock.patch("pynqc.Nqc.flash_and_exec_application", mock.Mock(side_effect=[True]))
    @mock.patch("pynqc.Nqc.set_program_slot", mock.Mock(side_effect=[True]))
    @mock.patch(
        "sw_upload.sw_upload_api._validate_interface",
        mock.Mock(side_effect=[(False, None, None, None), (True, None, None, None)]),
    )
    def test__upload_program(self):
        register_1 = 0
        register_2 = 1
        interface = {
            "id": {"register": register_1, "value": 0},
            "version": {"register": register_2, "value": 2},
        }
        program = {"slot": 1, "interface": interface, "path": "./program_path.nqc"}
        flags = ["-DTEST"]
        response = sw_upload._upload_program(nqc, program, flags)
        if not response == 0:
            assert False, f"{response}"

        expected_slot = [mock.call(1)]
        expected_flags = ["-DTEST"]
        nqc.set_program_slot.assert_has_calls(expected_slot)
        expected = [mock.call("./program_path.nqc", expected_flags)]
        nqc.flash_and_exec_application.assert_has_calls(expected)

    @mock.patch("pynqc.Nqc.set_program_slot", mock.Mock(side_effect=[True]))
    @mock.patch("pynqc.nqc._exec_nqc", mock.Mock(side_effect=[b""]))
    @mock.patch(
        "sw_upload.sw_upload_api._validate_interface",
        mock.Mock(side_effect=[(True, None, None, None)]),
    )
    def test__exec(self):
        callback = None
        register_1 = 0
        register_2 = 1
        interface = {
            "id": {"register": register_1, "value": 0},
            "version": {"register": register_2, "value": 2},
        }
        program = {
            "slot": 1,
            "interface": interface,
            "path": "./program_path.nqc",
            "monitor_state": [
                {"key": 0, "value": 1}
            ],  # callback is None so this will never be monitored
        }
        sw_upload._exec(nqc, program, callback)
        expected_slot = [mock.call(1)]
        nqc.set_program_slot.assert_has_calls(expected_slot)

    def test_build_flags(self):
        flags_map = [{"key": "-DTEST", "value": None}, {"key": "-DVALUE", "value": 1}]
        flags = sw_upload_cli.build_flags(flags_map)
        assert ["-DTEST", "-DVALUE=1"] == flags

    def test_validate_input(self, tmpdir):
        config_content = """
---
firmware:
  path: test
  version: test
program:
  interface:
    id_:
      register: 0
      value: 5
  slot: 1
  path: test
  flags:
  - key: test
    value: null
"""
        test_file = tmpdir.mkdir("dir").join("test.yml")
        test_file.write(config_content)

        Args = namedtuple(
            "Args",
            [
                "serial_type",
                "device_path",
                "config",
                "firmware_path",
                "program_path",
                "program_slot",
                "flags",
                "interface",
                "monitor_state",
            ],
        )

        expected_settings = {
            "device": "/dev/usb/legousbtower0",
            "firmware": {"path": "test", "version": "test"},
            "program": {
                "flags": [{"key": "test", "value": None}],
                "interface": {"id_": {"register": 0, "value": 5}},
                "monitor_state": [],
                "path": "test",
                "slot": 1,
            },
            "serial_type": "usb",
        }

        args = Args(
            serial_type=None,
            device_path=None,
            config=str(test_file),
            firmware_path=None,
            program_path=None,
            program_slot=1,
            flags=None,
            interface=None,
            monitor_state=None,
        )
        assert expected_settings == sw_upload_cli.validate_input(args)


# @pytest.mark.skipif(check_hw_availability(), reason="HW not available")
# class TestOnHwDUT:
#    @classmethod
#    def setup_class(cls):
#        pass
#
#    @classmethod
#    def teardown_class(cls):
#        path = "./firm0309.lgo"
#        exp_version = 3.9
#        response = rf.get_versions()
#        if not exp_version == float(response["desc-ram"]):
#            response = nqc.flash_firmware(path)
#        if not response:
#            assert response, "Teardown flashing failed"
#        response = rf.get_versions()
#        assert exp_version == float(response["desc-ram"])
#
#    def test_flash_firmware(self):
#        path = "./firm0328.lgo"
#        exp_version = 3.52
#        response = nqc.flash_firmware(path)
#        if not response:
#            assert response
#        response = rf.get_versions()
#        assert exp_version == float(response["desc-ram"])
#
#    def test_flash_and_exec_application(self):
#        ID_VAR = 0
#        VERSION_VAR = 1
#        EXP_ID = 253
#        EXP_VERSION = 255
#        path = "./test.nqc"
#        flags = ["-DOVERRIDE", f"-DID=2", f"-DVERSION=2"]
#        exp_flags = ["-DOVERRIDE", f"-DID={EXP_ID}", f"-DVERSION={EXP_VERSION}"]
#        response = nqc.flash_and_exec_application(path, flags)
#        if not response:
#            assert response
#        id_var = rf.get_value(_rf.VARIABLE, ID_VAR)
#        version_var = rf.get_value(_rf.VARIABLE, VERSION_VAR)
#
#        response = nqc.flash_and_exec_application(path, exp_flags)
#        new_id_var = rf.get_value(_rf.VARIABLE, ID_VAR)
#        new_version_var = rf.get_value(_rf.VARIABLE, VERSION_VAR)
#        assert (
#            id_var == 2
#            and version_var == 2
#            and new_id_var == 253
#            and new_version_var == 255
#        )
